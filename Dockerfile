FROM cirust_base:latest

RUN cargo install ripgrep
RUN cargo install fd-find
RUN cargo install cargo-local-registry
RUN cargo install cargo-lambda

RUN tar -cf cargo_home_backup.tar ${CARGO_HOME}
#RUN brotli -Z -o cargo_home_backup.tar.br --rm cargo_home_backup.tar
RUN lz4 -9 --rm cargo_home_backup.tar cargo_home_backup.tar.lz4
